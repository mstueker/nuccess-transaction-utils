//Copyright (C) 2016 Maik Stüker
//
//This file is part of Nuccess Transaction Utils.
//
//Nuccess Transaction Utils is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuccess Transaction Utils is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuccess Transaction Utils.  If not, see <http://www.gnu.org/licenses/>.
package de.nuccess.utils.transaction;

import org.nuclos.api.exception.BusinessException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
class TAUtilsService implements ApplicationContextAware {
	
	private static ApplicationContext appContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		appContext = applicationContext;
	}
	
	private static ApplicationContext getAppContext() {
		if (appContext == null) {
			throw new IllegalStateException("Too early");
		}
		return appContext;
	}
	
	static TAUtilsService getInstance() {
		return getAppContext().getBean(TAUtilsService.class);
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	public <T> T runInNewTransaction(TARunnableResult<T> runnable) throws BusinessException {
		return runnable.run();
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	public void runInNewTransaction(TARunnable runnable) throws BusinessException {
		runnable.run();
	}
	
}
