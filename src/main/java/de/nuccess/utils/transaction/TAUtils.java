//Copyright (C) 2024 Maik Stueker
//
//This file is part of Nuccess Transaction Utils.
//
//Nuccess Transaction Utils is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuccess Transaction Utils is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuccess Transaction Utils.  If not, see <http://www.gnu.org/licenses/>.
package de.nuccess.utils.transaction;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.exception.BusinessException;
import org.springframework.core.NamedThreadLocal;
import org.springframework.core.Ordered;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.util.Assert;

public class TAUtils {
	
	private static final Logger LOG = Logger.getLogger(TAUtils.class);
	
	private static final ThreadLocal<Map<String, Object>> TRANSACTIONAL_CACHE = new NamedThreadLocal<>("NuccessTransactionUtilsCache");
	
	private static final ThreadLocal<Integer> AFTER_ORDER = new ThreadLocal<>();
	
	private static final Map<String, Object> BLOCKS = new HashMap<>(); 
	
	private static final ThreadLocal<Object> CURRENT_BLOCK_OBJECT = new ThreadLocal<>();
	
	private static final int SYNC_ORDER_NEXT_AFTER_ORDER = Ordered.LOWEST_PRECEDENCE - 2;
	
	private static final int SYNC_ORDER_CLEAR_CACHE_VALUES = Ordered.LOWEST_PRECEDENCE - 1;
	
	private static final int SYNC_ORDER_RESET_BLOCKS = Ordered.LOWEST_PRECEDENCE;
	
	private static SecureRandom random;
	
	static {
		 try {
			random = SecureRandom.getInstance("SHA1PRNG");
		} catch (NoSuchAlgorithmException e) {
			random = new SecureRandom();
		}
	}
	
	/**
	 * Executes the runnable in a new transaction and returns the result, a thrown Business- or RuntimeException indicates a rollback.
	 *  Does not touch the current transaction.
	 * 
	 * @param runnable
	 * @return
	 * @throws BusinessException
	 */
	public static <T> T runInNewTransaction(TARunnableResult<T> runnable) throws BusinessException {
		return TAUtilsService.getInstance().runInNewTransaction(runnable);
	}
	
	/**
	 * Executes the runnable in a new transaction, a thrown Business- or RuntimeException indicates a rollback. 
	 * Does not touch the current transaction.
	 * 
	 * @param runnable
	 * @throws BusinessException
	 */
	public static void runInNewTransaction(TARunnable runnable) throws BusinessException {
		TAUtilsService.getInstance().runInNewTransaction(runnable);
	}
	
	private static int getNextOrder() {
		Integer order = AFTER_ORDER.get();
		if (order == null) {
			order = Ordered.HIGHEST_PRECEDENCE;
			TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
				@Override
				public void afterCompletion(int status) {
					AFTER_ORDER.remove();
				}
				@Override
				public int getOrder() {
					return SYNC_ORDER_NEXT_AFTER_ORDER;
				}
			});
		} else {
			order = order + 1;
		}
		AFTER_ORDER.set(order);
		return order;
	}
	
	/**
     * Executes the runnable in a new transaction after the current transaction is completed.
     *
     * @param runnable
     */
    public static void afterCompletion(final TARunnable runnable) {
        final int order = getNextOrder();
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
            @Override
            public void afterCompletion(int status) {
                try {
                    runInNewTransaction(runnable);
                } catch (BusinessException e) {
                    LOG.error(e.getMessage(), e);
                }
            }
            @Override
            public int getOrder() {
                return order;
            }
        });
    }
	
	/**
	 * Executes the runnable in a new transaction after the current transaction is rollbacked.
	 * 
	 * @param runnable
	 */
	public static void afterRollback(final TARunnable runnable) {
		final int order = getNextOrder();
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
			@Override
			public void afterCompletion(int status) {
				if (TransactionSynchronization.STATUS_ROLLED_BACK == status) {
					try {
						runInNewTransaction(runnable);
					} catch (BusinessException e) {
						LOG.error(e.getMessage(), e);
					}
				}
			}
			@Override
			public int getOrder() {
				return order;
			}
		});
	}
	
	/**
	 * Executes the runnable in a new transaction after the current transaction is committed.
	 * 
	 * @param runnable
	 */
	public static void afterCommit(final TARunnable runnable) {
		final int order = getNextOrder();
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
			@Override
			public void afterCommit() {
				try {
					runInNewTransaction(runnable);
				} catch (BusinessException e) {
					LOG.error(e.getMessage(), e);
				}
			}
			@Override
			public int getOrder() {
				return order;
			}
		});
	}

	/**
	 * Caches the given value along the current transaction is active. 
	 * Removes the value automatically from cache after completion. 
	 * 
	 * @param owner
	 * @param key
	 * @param value
	 */
	public static <T> void setCacheValue(Class<T> owner, String key, Object value) {
		Assert.notNull(value, "Value must not be null");
		String cacheKey = createCacheKey(owner, key);
		Map<String, Object> map = TRANSACTIONAL_CACHE.get();
		if (map == null) {
			map = new HashMap<>();
			TRANSACTIONAL_CACHE.set(map);
			TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
				@Override
				public void afterCompletion(int status) {
					Map<String, Object> map = TRANSACTIONAL_CACHE.get();
					if (map == null) {
						return;
					}
					TRANSACTIONAL_CACHE.remove();
				}
				@Override
				public int getOrder() {
					return SYNC_ORDER_CLEAR_CACHE_VALUES;
				}
			});
		}
		Object oldValue = map.put(cacheKey, value);
		if (oldValue != null) {
			throw new IllegalStateException("Already value '" + oldValue + "' for owner " + owner.getCanonicalName() + 
					" and key '" + key + 
					"' bound to thread [" + Thread.currentThread().getName() + "]");
		}
	}
	
	/**
	 * 
	 * @param owner
	 * @param key
	 * @return
	 */
	public static <T> Object getCacheValue(Class<T> owner, String key) {
		String cacheKey = createCacheKey(owner, key);
		Map<String, Object> map = TRANSACTIONAL_CACHE.get();
		if (map == null) {
			return null;
		}
		Object value = map.get(cacheKey);
		return value;
	}
	
	/**
	 * 
	 * @param owner
	 * @return
	 */
	public static <T> Map<String, Object> getCacheValues(Class<T> owner) {
		Assert.notNull(owner, "Owner must not be null");
		Map<String, Object> map = TRANSACTIONAL_CACHE.get();
		Map<String, Object> result = new HashMap<>();
		if (map != null) {
			final String cacheKeyPrefix = owner.getCanonicalName() + " ";
			for (Entry<String, Object> entry : map.entrySet()) {
				if (entry.getKey().startsWith(cacheKeyPrefix)) {
					result.put(entry.getKey().substring(cacheKeyPrefix.length()), entry.getValue());
				}
			}
		}
		return Collections.unmodifiableMap(result);
	}
	
	/**
	 * 
	 * @param owner
	 * @param key
	 */
	public static <T> void removeCacheValueIgnoreNull(Class<T> owner, String key) {
		removeCacheValue(owner, key, true);
	}
	
	/**
	 * 
	 * @param owner
	 * @param key
	 */
	public static <T> void removeCacheValue(Class<T> owner, String key) {
		removeCacheValue(owner, key, false);
	}
		
	private static <T> void removeCacheValue(Class<T> owner, String key, boolean ignoreNull) {
		String cacheKey = createCacheKey(owner, key);
		Map<String, Object> map = TRANSACTIONAL_CACHE.get();
		Object value = null;
		if (map != null) {
			value = map.remove(cacheKey);
			if (map.isEmpty()) {
				TRANSACTIONAL_CACHE.remove();
			}
		}
		if (!ignoreNull && value == null) {
			throw new IllegalStateException(
					"No value for owner " + owner.getCanonicalName() + " and key '" + key + 
					"' bound to thread [" + Thread.currentThread().getName() + "]");
		}
	}
	
	private static <T> String createCacheKey(Class<T> owner, String key) {
		Assert.notNull(owner, "Owner must not be null");
		Assert.notNull(key, "Key must not be null");
		return owner.getCanonicalName() + " " + key;
	}
	
	private static <T> String createBlockKey(Class<T> owner, Object id) {
		Assert.notNull(owner, "Owner must not be null");
		return owner.getCanonicalName() + " " + id == null ? "new" : ("id" + id);
	}
	
	/**
	 * Owner class and id is taken from BusinessObject, same as isBlocked(owner.getClass(), owner.getId()).
	 * 
	 * @param owner
	 * @return
	 */
	public static <T extends BusinessObject<?>> boolean isBlocked(T owner) {
		Assert.notNull(owner, "Owner must not be null");
		return isBlocked(owner.getClass(), owner.getId());
	}
	
	/**
	 * Use owner class only, same as isBlocked(ownerClass, null).
	 * 
	 * @param owner
	 * @return
	 */
	public static <T> boolean isBlocked(Class<T> owner) {
		return isBlocked(owner, null);
	}
	
	/**
	 * 
	 * @param ownerClass
	 * @param ownerId
	 * @return
	 */
	public static <T> boolean isBlocked(Class<T> ownerClass, Object ownerId) {
		return isBlocked(createBlockKey(ownerClass, ownerId));
	}
	
	private static boolean isBlocked(String blockKey) {
		synchronized (BLOCKS) {
			return BLOCKS.get(blockKey) != null;
		}
	}
	
	private static Object getOrCreateCurrentBlockObject() {
		synchronized (CURRENT_BLOCK_OBJECT) {
			Object blockObject = CURRENT_BLOCK_OBJECT.get();
			if (blockObject == null) {
				final byte[] blockData = new byte[20];
				random.nextBytes(blockData);
				blockObject = blockData;
				CURRENT_BLOCK_OBJECT.set(blockData);
			}
			return blockObject;
		}
	}
	
	/**
	 * Uses synchronization of threads. 
	 * If blocked by an other thread, a call would be synchronized and waits until the other block is released.
	 * Releases automatically when transaction is completed.
	 *
	 * Owner class and id is taken from BusinessObject, same as block(owner.getClass(), owner.getId()).
	 * 
	 * @param owner
	 */
	public static <T extends BusinessObject<?>> void block(T owner) {
		Assert.notNull(owner, "Owner must not be null");
		block(owner.getClass(), owner.getId());
	}
	
	/**
	 * Uses synchronization of threads. 
	 * If blocked by an other thread, a call would be synchronized and waits until the other block is released.
	 * Releases automatically when transaction is completed.
	 * 
	 * Use owner class only, same as block(ownerClass, null).
	 * 
	 * @param ownerClass
	 */
	public static <T> void block(Class<T> ownerClass) {
		block(ownerClass, null);
	}
	
	/**
	 * Uses synchronization of threads. 
	 * If blocked by an other thread, a call would be synchronized and waits until the other block is released.
	 * Releases automatically when transaction is completed.
	 * 
	 * @param ownerClass
	 * @param ownerId
	 */
	public static <T> void block(Class<T> ownerClass, Object ownerId) {
		final String blockKey = createBlockKey(ownerClass, ownerId);
		
		boolean newBlock = false;
		Object blockHolder = null;
		synchronized (BLOCKS) {
			blockHolder = BLOCKS.get(blockKey);
			if (blockHolder == null) {
				newBlock = true;
				blockHolder = getOrCreateCurrentBlockObject();
				BLOCKS.put(blockKey, blockHolder);
			}
		}
		final Object blockHolderFinal = blockHolder;
		
		if (newBlock) {
			TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
				@Override
				public void afterCompletion(int status) {
					synchronized (BLOCKS) {
						BLOCKS.remove(blockKey, blockHolderFinal);
					}
				}
				@Override
				public int getOrder() {
					return SYNC_ORDER_RESET_BLOCKS;
				}
			});
		} else {
			if (blockHolder != null) {
				Object currentBlockObject = getOrCreateCurrentBlockObject();
				if (!currentBlockObject.equals(blockHolder)) {
					while(blockHolder != null) {
						// wait for block release...
						try {
							Thread.sleep(500);
							synchronized (BLOCKS) {
								blockHolder = BLOCKS.get(blockKey);
							}
						} catch (InterruptedException e) {
							throw new RuntimeException(e);
						}
					}
					// synchronize again (necessary if more than one transaction waits for release)
					block(ownerClass, ownerId);
				}
			}
		}
	}
	
}
