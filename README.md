# Nuccess Transaction Utils

for Nuclos (http://www.nuclos.de/)

Usable in any Nuclos server rule. See Javadoc or Example Nuclet for more information. 

## Links

* Download Jar or Nuclet https://bitbucket.org/mstueker/nuccess-transaction-utils/downloads
* Javadoc http://nuccess.de/nuccess-utils/javadoc/master/
* Maven Repository https://bitbucket.org/mstueker/nuccess-utils-maven-repo/raw/master/repository/

## How can I use it in my Maven projects?

Add a dependency element and the repository to your pom:

```
#!xml

<repository>
	<id>nuccess-utils</id>
	<url>https://bitbucket.org/mstueker/nuccess-utils-maven-repo/raw/master/repository/</url>
</repository>

<dependency>
	<groupId>de.nuccess.utils</groupId>
	<artifactId>nuccess-transaction-utils</artifactId>
    <version>1.1</version>
</dependency>
```
## License

Nuccess Transaction Utils is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nuccess Transaction Utils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Nuccess Transaction Utils.  If not, see <http://www.gnu.org/licenses/>.



*Copyright (C) 2024 Maik Stueker*